
<html> 
<head>

<title> RESUME </title>

<style type ="text/css">

.greenname { color:green;}
.mobilenum { color:darkred;}
.careerobj { color: green;}
.seek { color:maroon;}
.techskills {color:green;}
.hardware { color:maroon;}
.persoskills { color:green;}
.anothercolor {color:ivory;}

h1,p,h2,table,ul,li { text-align: center;}
li {align-items: center;}
td{align-items: flex-end;}

body { background-image: url(bgc.jpg);
        background-position:center;
        background-color: lightgreen;}
        
</style>

</head> 

<body>
    <img src="emem.jpg" height="150" />
    <h1 class="greenname"> Emmabelle L. Alcuizar </h1>
    <p class="mobilenum">  Mobile number: 09667584257 <br/>
         Email: emmabelleloqs@gmail.com <br/>
         Address: Purok 3, Brgy. 37-D, Artiaga Extension, Quezon Boulevard, Davao City <br/>
         Date of Birth: March 6, 1998</p> <hr/>

    <h2 class="careerobj"> CAREER OBJECTIVE </h2>
    <p class="seek"> Seeking a challenging career with a progressive 
        organization <br/> that provides an opportunity to capitalize 
        my technical skills and abilities.</p>
        <hr/>

        <h2 class="techskills"> TECHNICAL SKILLS </h2>
     
        <ul> 
            <li class="hardware"> Hardware troubleshooting </li>
            <li class="hardware"> Adobe Creative Suite( Photoshop ) </li>
            </ul>

        <h2 class="persoskills"> PERSONAL SKILLS </h2>
            <ul> 
                <li class="hardware">Highly organized and efficient</li>
                <li class="hardware">Ability to work independently or as part of the team</li>
                <li class="hardware">Self-motivation and decision making</li>
                <li class="hardware"> Ability to Work Under Pressure.</li>
                <li class="hardware"> Adaptability</li>
            </ul>
            <hr/>

        <h2 class="persoskills"> EDUCATION BACKGROUND </h2>
            
        <table border="2" align="center">
    
            <tr>
                <th class="persoskills"> Level </th>
                <th class="persoskills"> School </th>
                <th class="persoskills"> Year </th>
            </tr>
            <tr>
                <td class="anothercolor"> Tertiary <br/> Bachelor of Science in Information Technology </td>
                <td class="anothercolor"> University of Southeastern Philippines </td>
                <td class="anothercolor"> 2016-2018 </td>
            </tr>
            <tr>
                <td class="anothercolor"> Secondary </td>
                <td class="anothercolor"> Sinawilan National High School </td>
                <td class="anothercolor"> 2013-2014 </td>
            </tr>
            <tr>
                <td class="anothercolor"> Primary </td>
                <td class="anothercolor"> Sinawilan Elementary School </td>
                <td class="anothercolor"> 2009-2010 </td>
            </tr>
 </table>
 <hr/>

     <h2 class="techskills"> PERSONAL DATA </h2>
     <table border="1" align="center">
        <tr>
            <td class="anothercolor"> Date of Birth: March 6, 1998 </td>
            <td class="anothercolor"> Religion: Roman Catholic </td>
            <td class="anothercolor"> Height: 152.4 cm </td>

        </tr>
        <tr>
            <td class="anothercolor"> Place of Birth: Sinawilan, Matanao, Davao del Sur </td>
            <td class="anothercolor"> Sex: Female </td>
            <td class="anothercolor"> Weight: 44 kg </td>

        </tr>
        <tr>
            <td class="anothercolor"> Age: 20 years-old </td>
            <td class="anothercolor"> Civil Status: Single </td>
            <td class="anothercolor"> Language: Tagalog, English </td>
 </tr>
    </table>
    <hr/>

    <h2 class="techskills"> REFFERENCE </h2>
    <p class="hardware"> Christopher Gorra (Area Manager Activation Machine Inc.,09305203809) <br/>
        Dr. Winston Cervantes (Doctor of Makati Medical, 09105529942) 
    </p>



</body>
</html>
